﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CPh.Parking.Web.Data;
using CPh.Parking.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;

namespace CPh.Parking.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Sample")]
    public class SampleController : Controller
    {
        private readonly ParkingDbContext _SDB;

        public SampleController(ParkingDbContext SDB)
        {
            _SDB = SDB;
        }



        [HttpGet]
        public string Exports()
        {
            string sample = @"Sample.xlsx";
            string path = "D:\\";
            string URL = string.Format("{0}://{1}/{2}", Request.Scheme, Request.Host, sample);
            FileInfo info = new FileInfo(Path.Combine(path, sample));

            if (info.Exists)
            {
                info.Delete();
                info = new FileInfo(Path.Combine(path, sample)); 
            }
             


            using (ExcelPackage package = new ExcelPackage(info))
            {
                IList<User> users = _SDB.Users.ToList();
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("User");
                int totalrows = users.Count();

                worksheet.Column(1).Width = 25;
                worksheet.Column(2).Width = 25;
                worksheet.Column(3).Width = 25;
                worksheet.Column(4).Width = 25;
                worksheet.Column(5).Width = 25;
                worksheet.Column(6).Width = 25;
                worksheet.Column(7).Width = 25;
                worksheet.Column(8).Width = 25;
                worksheet.Column(9).Width = 25;

                //worksheet.Column(1).alignment = 25;

                worksheet.Cells[1, 1].Value = "Global ID";
                worksheet.Cells[1, 2].Value = "Last Name;";
                worksheet.Cells[1, 3].Value = "First Name";
                worksheet.Cells[1, 4].Value = "Position";
                worksheet.Cells[1, 5].Value = "Hire Date";
                worksheet.Cells[1, 6].Value = "Employee Status";
                worksheet.Cells[1, 7].Value = "Current Location";
                worksheet.Cells[1, 8].Value = "Floor";
                worksheet.Cells[1, 9].Value = "IP Phone";

                int i = 0;
                for (int row = 2; row <= totalrows+1; row++)
                {

                    for (int m = 1; m <= 9; m++)
                    {
                        worksheet.Cells[1, m].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        worksheet.Cells[1, m].Style.Fill.BackgroundColor.SetColor(Color.LightSteelBlue);
                    }

                    worksheet.Cells[row, 1].Value = users[i].GlobalID;
                    worksheet.Cells[row, 2].Value = users[i].LastName;
                    worksheet.Cells[row, 3].Value = users[i].FirstName;
                    worksheet.Cells[row, 4].Value = users[i].Position;
                    worksheet.Cells[row, 5].Value = users[i].HireDate;
                    worksheet.Cells[row, 6].Value = users[i].EmpStat;
                    worksheet.Cells[row, 7].Value = users[i].CurLoc;
                    worksheet.Cells[row, 8].Value = users[i].Floor;
                    worksheet.Cells[row, 9].Value = users[i].IP;
                   

                    i++;
                   
                }
                
                    package.Save();
            }

            return "Saving.......";

        }
       
           


    }
}