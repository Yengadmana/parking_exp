﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CPh.Parking.Web.Controllers
{
    [Authorize]
    public class AdministrationController : Controller
    {
        [Authorize(Roles = "CPh-F-ORG_HMR_Dispatch_Gcp-M,CPh-F-ORG_Itm_Pit_EngIt_Im-R")]
        public IActionResult Index()
        {
            return View();
        }
    }
}