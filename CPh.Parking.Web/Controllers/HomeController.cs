﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CPh.Parking.Web.Models;
using CPh.Parking.Web.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;

namespace CPh.Parking.Web.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ParkingDbContext _context;

        public HomeController(ParkingDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            
            return RedirectToAction("ParkingSlots");
        }
        public async Task<IActionResult> ParkingSlots(int? id)
        {
            if (id == null)
            {
                return View();
            }

            var user = await _context.Users.SingleOrDefaultAsync(m => m.Id == id);
            if (user == null)
            {
                return NotFound();
            }
            //Response.Redirect("/Home/ParkingSlots?id=" + id);
            return View(user);
        }



        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        //[Authorize(Roles = "CPh-F-ORG_HMR_Dispatch_Gcp-M,CPh-F-ORG_Itm_Pit_EngIt_Im-R")]
        //public IActionResult Contact()
        //{
        //    ViewData["Message"] = "Your contact page.";

        //    return View();
        //}

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        
        
        //public IActionResult ParkingSlots()
        //{
        //    return View();
        //}
    }
}
