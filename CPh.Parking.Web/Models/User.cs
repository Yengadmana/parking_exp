﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CPh.Parking.Web.Models
{
    public class User
    {
        [Required]
        [Display(Name = "Parking SLot No.")]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Global ID")]
        public string GlobalID { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }


        [Display(Name = "Position")]
        public string Position { get; set; }

        [Display(Name = "Hire Date")]
        [Required]
        [DataType(DataType.Date)]
        public DateTime HireDate { get; set; }

        [Display(Name = "Employee Status")]
        public Status EmpStat { get; set; }

        [Display(Name = "Current Location")]
        public string CurLoc { get; set; }

        [Display(Name = "Floor")]
        public int Floor { get; set; }

        [Display(Name = "IP Phone")]
        public string IP { get; set; }

        public virtual ICollection<Vehicle> Vehicles { get; set; }

        public enum Status
        {
            Regular,
            Probationary
        }
        
      
    }
}
